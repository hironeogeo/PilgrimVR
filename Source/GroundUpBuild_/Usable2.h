// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "CoreMinimal.h"
#include "Engine.h"
//#include "Interface.h"
#include "Usable2.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UUsable2 : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GROUNDUPBUILD__API IUsable2
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//This function will be called when the user uses the object
	virtual void OnUsed(AController * user);
	// This function is called each frame from the hud, it should be used to put messages to the screen, like the USE promt in UDK
	virtual void DisplayPrompt(UCanvas * Canvas, AController * user);
	
};
