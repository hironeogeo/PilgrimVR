// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Runtime/Engine/Classes/Components/BoxComponent.h"

//#include <set>

#include "Runtime/Core/Public/Containers/Set.h"
#include "Runtime/Core/Public/Containers/Array.h"

#include "SpatialNode.generated.h"

//typedef int spatialNodeID;

UCLASS()
class GROUNDUPBUILD__API ASpatialNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpatialNode();
	~ASpatialNode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(Category = "Character", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* worldRepresentation;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// function to return the connected nodes for this node
	//std::set<spatialNodeID> getConnectedNodes();
	//std::set<int> getConnectedNodes();
	TSet<int> ASpatialNode::getConnectedNodes();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialNode_Components")
	int m_ID;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialNode_Components")
	//FVector m_pos;

	//UPROPERTY(VisibleDefaultsOnly, Category = "SpatialNode_Components")
	//USkeletalMeshComponent* worldRepresentation;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialNode_Components")
	//UStaticMesh* staticVersionWolrdRepresentation;

	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return worldRepresentation; }

	//std::set<spatialNodeID> m_connectedNodes;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialNode_Components")
	TSet<int> m_connectedNodes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialNode_Components")
	TArray<ASpatialNode*> realConnectedNodes;
	//std::set<int> m_connectedNodes;

	bool m_isVisible;
	bool m_interactable;
};
