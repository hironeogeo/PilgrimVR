// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Transporter.h"
#include "EDP.generated.h"

UCLASS()
class GROUNDUPBUILD__API AEDP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEDP();

	// timer to determine when the automated progression happens
	FTimerHandle progressionTimer;

	// the box collider responsible for triggering the video to start
	UPROPERTY(Category = "EDP_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* boxComponent;

	// the actual lenght of the timer - calculated externally by using the video file
	UPROPERTY(Category = "EDP_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float progressionDelay;

	// the transporter responsible for controling the level switching and player moving
	UPROPERTY(Category = "EDP_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	ATransporter* transportationDevice;

	// 
	UFUNCTION(BlueprintCallable, Category = "EDP_Tools")
	FVector advanceProgression();
	
	// to be called once the video has started 
	UFUNCTION(BlueprintCallable, Category = "EDP_Tools")
	void startTimer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
