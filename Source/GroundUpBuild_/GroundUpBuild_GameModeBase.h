// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GroundUpBuild_GameModeBase.generated.h"


/**
 * 
 */
UCLASS()
class GROUNDUPBUILD__API AGroundUpBuild_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
		virtual void StartPlay() override;
	
	
};


