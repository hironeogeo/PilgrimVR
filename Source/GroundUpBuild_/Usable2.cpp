// Fill out your copyright notice in the Description page of Project Settings.

#include "Usable2.h"

UUsable2::UUsable2(UUsable2&&)
{
	
}

//This is required for compiling,  and its the base version, you can put something here and it will be the default behaviour
void IUsable2::OnUsed(AController* user)
{
	
}

void IUsable2::DisplayPrompt(UCanvas* Canvas, AController* user)
{
	
}

// Add default functionality here for any IUsable2 functions that are not pure virtual.
