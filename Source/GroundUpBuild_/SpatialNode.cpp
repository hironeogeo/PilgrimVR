// Fill out your copyright notice in the Description page of Project Settings.

#include "SpatialNode.h"
//#include "SpatialGraph.h"

// Sets default values
ASpatialNode::ASpatialNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_isVisible = true;
	m_interactable = true;
	m_ID = 0; //USpatialGraph::s_SpatialGraph->getNumNodes() + 1;
	//USpatialGraph::s_SpatialGraph->addNode(m_ID, this);

	// Create a  mesh component for the owning player.
	worldRepresentation = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpatialNodeMesh"));
	RootComponent = worldRepresentation;
	
	// Only the owning player sees this mesh.
	//worldRepresentation->SetOnlyOwnerSee(true);

	// Attach the FPS mesh to the FPS camera.
	//FPSMesh->SetupAttachment(FPSCameraComponent);
	// Disable some environmental shadowing to preserve the illusion of having a single mesh.
	//worldRepresentation->bCastDynamicShadow = false;
	//worldRepresentation->CastShadow = false;

	

//	worldRepresentation->SetupAttachment();

	worldRepresentation->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	worldRepresentation->SetCollisionObjectType(ECC_GameTraceChannel2);

	//staticVersionWolrdRepresentation = CreateDefaultSubobject<UStaticMesh>(TEXT("TestMesh"));
	//staticVersionWolrdRepresentation->setSetOnlyOwnerSee(true);
	//staticVersionWolrdRepresentation->bCastDynamicShadow = false;
	//staticVersionWolrdRepresentation->CastShadow = false;

	//staticVersionWolrdRepresentation->

	// The owning player doesn't see the regular (third-person) body mesh.
	//GetMesh()->SetOwnerNoSee(true);
}

ASpatialNode::~ASpatialNode()
{

}

// Called when the game starts or when spawned
void ASpatialNode::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASpatialNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//std::set<spatialNodeID> ASpatialNode::getConnectedNodes()
//std::set<int> ASpatialNode::getConnectedNodes()
TSet<int> ASpatialNode::getConnectedNodes()
{
	return m_connectedNodes;
}

