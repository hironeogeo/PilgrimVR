// Fill out your copyright notice in the Description page of Project Settings.

#include "EDP.h"


// Sets default values
AEDP::AEDP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	progressionDelay = 0.0f;

	transportationDevice = CreateDefaultSubobject<ATransporter>(TEXT("transportationDevice"));

	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("boxComponent"));

}

// Called when the game starts or when spawned
void AEDP::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEDP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEDP::startTimer()
{
	//if (progressionDelay > 0.0f)
	//{
	//	GetWorldTimerManager().SetTimer(progressionTimer, this, &AEDP::advanceProgression, progressionDelay, false);
	//}
}

FVector AEDP::advanceProgression()
{
	FVector newPlayerLoc = transportationDevice->transportTo();

	return newPlayerLoc;
}

