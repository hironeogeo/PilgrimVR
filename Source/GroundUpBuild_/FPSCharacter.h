// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
//#include "Usable.h"
#include "CoreMinimal.h"
#include "Usable2.h"
#include "GameFramework/Character.h"

#include "SpatialNode.h"

#include "FPSCharacter.generated.h"


UCLASS()
class GROUNDUPBUILD__API AFPSCharacter : public ACharacter, public IUsable2
{
GENERATED_BODY()

private:
	bool isMovingToNode;
	FVector targetPos;
	FTimerHandle movementTimer;
	float movementDuration;

public:
	// Sets default values for this character's properties
	AFPSCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Handles input for moving forward and backward.
	UFUNCTION()
	void MoveForward(float Value);

	// Handles input for moving right and left.
	UFUNCTION()
	void MoveRight(float Value);
	// Sets jump flag when key is pressed.

	UFUNCTION()
	void RotatePlayerRight(float Value);

	UFUNCTION()
	void RotatePlayerLeft(float Value);

	UFUNCTION()
	void StartJump();
	
	// Clears jump flag when key is released.
	UFUNCTION()
	void StopJump();

	UFUNCTION()
	void stopTimer();

	// use function
	UFUNCTION(BlueprintCallable, Category = Hability)
	virtual void TestPlayerAction();

	//std::set<spatialNodeID> m_connectedNodes;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player_settings")
	int nodeToStartOn;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player_settings")
	float playerInteractionDistance;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player_settings")
	float blinkSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player_settings")
	float rotateSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player_settings")
	bool debugControls;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player_settings")
	bool standardControls;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player_settings")
	ASpatialNode* currentNode;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player_settings")
	ASpatialNode* previousNode;

};
