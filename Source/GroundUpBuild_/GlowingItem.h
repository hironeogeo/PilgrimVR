// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "GlowingItem.generated.h"

UCLASS()
class GROUNDUPBUILD__API AGlowingItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGlowingItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Toggle Item Glow
	UFUNCTION(BlueprintCallable, Category="ItemGlow")
	void ToggleGlow(bool isGlowing);
	UFUNCTION(BlueprintCallable, Category="ItemGlow")
		void EnteredObjectRadius(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult);
	UFUNCTION(BlueprintCallable, Category="ItemGlow")
		void LeftObjectRadius(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex);
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="GlowingItem")
		UStaticMeshComponent* SM_Pickup;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GlowingItem")
		UBoxComponent* BT_Pickup;
};
