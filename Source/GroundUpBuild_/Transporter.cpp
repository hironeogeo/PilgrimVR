// Fill out your copyright notice in the Description page of Project Settings.

#include "Transporter.h"
#include "Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Core/Public/Delegates/DelegateSignatureImpl.inl"
#include <algorithm>


// Sets default values
ATransporter::ATransporter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	worldRepresentation = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TransporterMesh"));
	//trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("TransporterBoxComponent"));
	transportDestination = CreateDefaultSubobject<ASpatialNode>(TEXT("TransporterDestination"));
	RootComponent = worldRepresentation;
	worldRepresentation->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	worldRepresentation->SetCollisionObjectType(ECC_GameTraceChannel3);

	unloadDelay = 0.000005f;
	
	levelStreamed = false;
}

ATransporter::~ATransporter()
{

}

// Called when the game starts or when spawned
void ATransporter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATransporter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// function responisble for changing level and moving the player to the level start point

FVector ATransporter::transportTo()
{

	// trigger the fade out to start

	if (levelToLoad != "")
		loadLevel(levelToLoad);

	for (int i = 0; i < levelsToUnload.Num(); ++i)
	{
		// create the queue of levels to unload
		levelUnloadQueue.push_back(levelsToUnload[i]);
	}

	if(levelUnloadQueue.size() > 0)
		GetWorldTimerManager().SetTimer(unloadTimer, this, &ATransporter::UnloadLevel, unloadDelay, false);
	// transport the player to the new level

	transportDestination->SetActorHiddenInGame(true);
	transportDestination->m_interactable = false;
	transportDestination->m_isVisible = false;

	for (int i = 0; i < transportDestination->realConnectedNodes.Num(); ++i)
	{
		transportDestination->realConnectedNodes[i]->SetActorHiddenInGame(false);
		transportDestination->realConnectedNodes[i]->m_interactable = true;
		transportDestination->realConnectedNodes[i]->m_isVisible = true;
	}

	return transportDestination->GetActorLocation();
}

void ATransporter::loadLevel(FName level)
{
	FLatentActionInfo LatentInfo;
	UGameplayStatics::LoadStreamLevel(this, levelToLoad, true, true, LatentInfo);
}

void ATransporter::UnloadLevel()
{
	FLatentActionInfo LatentInfo2;

	// unload the first level in the queue
	UGameplayStatics::UnloadStreamLevel(this, levelUnloadQueue[0], LatentInfo2);

	// erase the first element
	levelUnloadQueue.erase(levelUnloadQueue.begin());

	// if there are more elements then repeate the process - acts recursively
	if(levelUnloadQueue.size() > 0)
		GetWorldTimerManager().SetTimer(unloadTimer, this, &ATransporter::UnloadLevel, unloadDelay, false);
}

ASpatialNode* ATransporter::getTransportDestination()
{
	return transportDestination;
}