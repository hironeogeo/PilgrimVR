// Fill out your copyright notice in the Description page of Project Settings.

#include "GlowingItem.h"


// Sets default values
AGlowingItem::AGlowingItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SM_Pickup = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SMPickup"));

	BT_Pickup = CreateDefaultSubobject<UBoxComponent>(TEXT("BTPickup"));
	BT_Pickup->bGenerateOverlapEvents = true;
	BT_Pickup->OnComponentBeginOverlap.AddDynamic(this, &AGlowingItem::EnteredObjectRadius);
	BT_Pickup->OnComponentEndOverlap.AddDynamic(this, &AGlowingItem::LeftObjectRadius);

}

// Called when the game starts or when spawned
void AGlowingItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGlowingItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGlowingItem::ToggleGlow(bool isGlowing)
{
	SM_Pickup->SetRenderCustomDepth(isGlowing);
}

void AGlowingItem::EnteredObjectRadius(UPrimitiveComponent * hitComp, AActor * otherActor, UPrimitiveComponent * otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult & sweepResult)
{
	ToggleGlow(true);
}

void AGlowingItem::LeftObjectRadius(UPrimitiveComponent * hitComp, AActor * otherActor, UPrimitiveComponent * otherComp, int32 otherBodyIndex)
{
	ToggleGlow(false);
}




