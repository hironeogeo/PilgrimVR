//#pragma once
//
//#include "CoreMinimal.h"
//#include "Engine/Canvas.h"
////#include "Interface.h"
//
//#include "Engine.h"
//
//#include "Usable.generated.h"
//
////UINTERFACE(MinimalAPI)
////class GROUNDUPBUILD__API UUsable :public UInterface
////{
////	GENERATED_UINTERFACE_BODY()
////
////	UUsable(const  class FPostConstructInitializeProperties& PCIP);
////};
//
//class GROUNDUPBUILD__API IUsable //: public UInterface
//{
//	GENERATED_IINTERFACE_BODY()
//	
//
////public:
//
//	IUsable();
//	//virtual ~IUsable();
//	// This function will be called when the user uses the object
//	virtual void OnUsed(AController * user);
//	// This function is called each frame from the hud, it should be used to put messages to the screen, like the USE promt in UDK
//	virtual void DisplayPrompt(UCanvas * Canvas, AController * user);
//};