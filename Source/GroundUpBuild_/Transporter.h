// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "SpatialNode.h"
#include <vector>
#include "Transporter.generated.h"

UCLASS()
class GROUNDUPBUILD__API ATransporter : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATransporter();
	~ATransporter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(Category = "Transporter_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* worldRepresentation;

	//UPROPERTY(Category = "Transporter_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	//UBoxComponent* trigger;
	
	UPROPERTY(Category = "Transporter_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	ASpatialNode* transportDestination;

	UPROPERTY(Category = "Transporter_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FName levelToLoad;

	UPROPERTY(Category = "Transporter_Properties", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<FName> levelsToUnload;

	bool levelStreamed;
	FTimerHandle unloadTimer;
	float unloadDelay;

	std::vector<FName>levelUnloadQueue;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Transporter Functions")
	FVector transportTo(); // returns the start location in the new level

	UFUNCTION()
	void loadLevel(FName level);

	UFUNCTION()
	void UnloadLevel();

	ASpatialNode* getTransportDestination();

};
