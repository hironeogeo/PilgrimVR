// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSCharacter.h"
#include "Transporter.h"
#include "Runtime/CoreUObject/Public/Templates/Casts.h"


// Sets default values
AFPSCharacter::AFPSCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	debugControls = true;
	standardControls = true;
	isMovingToNode = false;
	
	playerInteractionDistance = 2000;
	movementDuration = 0.6f;
	blinkSpeed = 20.0f;
	rotateSpeed = 2.8f;

	currentNode = nullptr;
}

// Called when the game starts or when spawned
void AFPSCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isMovingToNode)
	{
		if (GetActorLocation() != targetPos)
		{
			SetActorLocation(FMath::VInterpTo(GetActorLocation(), targetPos, DeltaTime, blinkSpeed));
		}
		else
			isMovingToNode = false;
	}

}

void AFPSCharacter::TestPlayerAction()
{
	if (Controller == NULL) // we access the controller, make sure we have one, else we will crash
	{
		return;
	}

	FVector CamLoc;
	FRotator CamRot;
	Controller->GetPlayerViewPoint(CamLoc, CamRot);
	const FVector StartTrace = CamLoc;
	const FVector ShootDir = CamRot.Vector();
	//const FVector EndTrace = StartTrace + ShootDir * 200;
	const FVector EndTrace = StartTrace + ShootDir * playerInteractionDistance;

	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(FName(TEXT("RayTraceFromCamera")), true, this);
	const FCollisionObjectQueryParams transporterObj(ECC_GameTraceChannel3); // transporter object
	const FCollisionObjectQueryParams movementNodes(ECC_GameTraceChannel2); // movement nodes
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	bool hitResult;

	FHitResult Hit(ForceInit);

	// check for Spatial Node
	hitResult = GetWorld()->LineTraceSingleByObjectType(Hit, StartTrace, EndTrace, movementNodes, TraceParams);

	if (hitResult)
	{
		ASpatialNode* newNode = dynamic_cast<ASpatialNode*>(Hit.GetActor());

		if (newNode->m_interactable)
		{
			isMovingToNode = true;
			targetPos = Hit.GetActor()->GetActorLocation();

			// hide the node we're moving to
			newNode->SetActorHiddenInGame(true);
			newNode->m_isVisible = false;
			newNode->m_interactable = false;

			if (currentNode != nullptr)
			{
				previousNode = currentNode;
				// hide the previous node
				currentNode->SetActorHiddenInGame(true);
				currentNode->m_isVisible = false;
				currentNode->m_interactable = false;

				// hide all the formally connected nodes
				for (int i = 0; i < currentNode->realConnectedNodes.Num(); ++i)
				{
					currentNode->realConnectedNodes[i]->SetActorHiddenInGame(true);
					currentNode->realConnectedNodes[i]->m_isVisible = false;
					currentNode->realConnectedNodes[i]->m_interactable = false;
				}
			}

			for (int i = 0; i < newNode->realConnectedNodes.Num(); ++i)
			{

				// make visible the newly connected nodes
				newNode->realConnectedNodes[i]->SetActorHiddenInGame(false);
				newNode->realConnectedNodes[i]->m_isVisible = true;
				newNode->realConnectedNodes[i]->m_interactable = true;
			}

			currentNode = newNode;
			SetActorEnableCollision(false);

			// set the players current node to the collided actor

			GetWorldTimerManager().SetTimer(movementTimer, this, &AFPSCharacter::stopTimer, movementDuration, false);
		}
	}
	else
	{
		// check for a Transporter Object
		hitResult = GetWorld()->LineTraceSingleByObjectType(Hit, StartTrace, EndTrace, transporterObj, TraceParams);

		if (hitResult)
		{
			ATransporter* transporter = dynamic_cast<ATransporter*>(Hit.GetActor());
			FVector newlocation = transporter->transportTo();

			if (currentNode == nullptr)
			{
				currentNode = transporter->getTransportDestination();
				previousNode = currentNode;
			}
			else
			{
				previousNode = currentNode;
				currentNode = transporter->getTransportDestination();
			}

			SetActorLocation(newlocation);
		}
	}
}

// Called to bind functionality to input
void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up "movement" bindings.
	PlayerInputComponent->BindAxis("MoveForward", this, &AFPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFPSCharacter::MoveRight);

	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &AFPSCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AFPSCharacter::AddControllerPitchInput);


	//&AFPSCharacter::AddControllerRollInput

	PlayerInputComponent->BindAxis("RotateRight", this, &AFPSCharacter::RotatePlayerRight);
	PlayerInputComponent->BindAxis("RotateLeft", this, &AFPSCharacter::RotatePlayerLeft);

	// Set up "action" bindings.
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacter::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AFPSCharacter::StopJump);

	PlayerInputComponent->BindAction("Player_Action", IE_Pressed, this, &AFPSCharacter::TestPlayerAction);
}

void AFPSCharacter::MoveForward(float Value)
{
	if (debugControls)
	{
		// Find out which way is "forward" and record that the player wants to move that way.
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AFPSCharacter::MoveRight(float Value)
{
	if (debugControls)
	{
		// Find out which way is "right" and record that the player wants to move that way.
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void AFPSCharacter::RotatePlayerRight(float value)
{
	if(standardControls)
		AddControllerYawInput(value * rotateSpeed);
}

void AFPSCharacter::RotatePlayerLeft(float value)
{
	if (standardControls)
		AddControllerYawInput(value * -rotateSpeed);
}

void AFPSCharacter::StartJump()
{
	if (debugControls)
		bPressedJump = true;
}

void AFPSCharacter::StopJump()
{
	if (debugControls)
		bPressedJump = false;
}

void AFPSCharacter::stopTimer()
{
	//GetWorldTimerManager().GetTimerElapsed(movementTimer);
	isMovingToNode = false;
	SetActorEnableCollision(true);
}