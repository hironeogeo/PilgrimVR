// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class GroundUpBuild_EditorTarget : TargetRules
{
	public GroundUpBuild_EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "GroundUpBuild_" } );
	}
}
